import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Facebook } from '@ionic-native/facebook';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login'
import { StartingPage } from '../pages/starting/starting'
import { ViewPage } from '../pages/view/view'
import { Page1Page } from '../pages/page1/page1'
import { Page2Page } from '../pages/page2/page2'
import { Page3Page } from '../pages/page3/page3'
import { AccountSettingsPage } from '../pages/account-settings/account-settings'
import {HowToRedeemPage} from '../pages/how-to-redeem/how-to-redeem';

import { EventModalPage } from '../pages/event-modal/event-modal'
import { EmailRegistrationPage } from '../pages/email-registration/email-registration'

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyCbBwGqKJtx3vxWtjAgcQunCuwcuweyqVU",
  authDomain: "test-6c703.firebaseapp.com",
  databaseURL: "https://test-6c703.firebaseio.com",
  storageBucket: "test-6c703.appspot.com",
  messagingSenderId: '683443866568'
};

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    StartingPage,
    ViewPage,
    Page1Page,
    Page2Page,
    Page3Page,
    EventModalPage,
    EmailRegistrationPage,
    AccountSettingsPage,
    HowToRedeemPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    StartingPage,
    ViewPage,
    Page1Page,
    Page2Page,
    Page3Page,
    EventModalPage,
    EmailRegistrationPage,
    AccountSettingsPage,
    HowToRedeemPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Facebook,
  ]
})
export class AppModule {}
