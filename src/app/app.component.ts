import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ViewPage } from '../pages/view/view';
import { StartingPage } from '../pages/starting/starting'
import { AngularFireAuth } from 'angularfire2/auth';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

 rootPage:any;


  constructor(platform: Platform, private afAuth: AngularFireAuth, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.afAuth.authState
    .subscribe(
      user => {
        if (user) {
          this.rootPage = ViewPage;
        } else {
          this.rootPage = StartingPage;
        }
      }, () => {
        this.rootPage = StartingPage;
      })
  }

}
