import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AccountSettingsPage } from '../account-settings/account-settings'

/**
 * Generated class for the Page1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html',
})
export class Page1Page {

  //Test data to render on the page.
  public testArr = [
    {
      img: 'http://cdn.shopify.com/s/files/1/1090/3962/articles/buy_one_get_one_free_1200x1200.png?v=1463631389',
      description : 'First description.',
      time : 3,
    },
    {
      img : 'http://cdn-static.buy1get1.in/upload/offer/Chai_Point_Discount_Code_Flat_25_off_on_all_orders.png',
      description : 'Second description.',
      time : 4,
    },
    {
      img : 'http://www.uidownload.com/files/653/264/53/coupon-discount-ecommerce-price-sale-shopping-icon.png',
      description : 'Third description.',
      time : 5,
    }
  ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.testArr = this.testArr;

  }

  ionViewDidLoad() {

  }

  goToAccount(){
      this.navCtrl.push(AccountSettingsPage);
  }

}
