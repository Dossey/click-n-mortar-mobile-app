import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmailRegistrationPage } from './email-registration';

@NgModule({
  declarations: [
    EmailRegistrationPage,
  ],
  imports: [
    IonicPageModule.forChild(EmailRegistrationPage),
  ],
})
export class EmailRegistrationPageModule {}
