import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { User } from "../../shared/models/user";
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController } from 'ionic-angular';
import { ViewPage } from '../view/view'
import { LoginPage } from '../login/login'

/**
 * Generated class for the EmailRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.

 */

@IonicPage()
@Component({
  selector: 'page-email-registration',
  templateUrl: 'email-registration.html',
})
export class EmailRegistrationPage {

  //User object to store the email and the passwo
  
  public Form: any = {};

  constructor(
    private afAuth: AngularFireAuth, 
    private alertCtrl : AlertController,
    public navCtrl: NavController, 
    public navParams: NavParams
 ){ 


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.Form = this.Form;


  }


  //The function to SIGN UP with email.
  emailSignUp() {
        //Variable that checks the passwords
    let checkPasswords = (this.Form.password === this.Form.confirmPassword);

    //checking to see if 
    let checkString = (this.Form.password != undefined && this.Form.confirmPassword != undefined && this.Form.email != undefined)

    //Check if the email and password is undefined.
    if(!checkPasswords){

      //Give an error for register failing.
      this.registerFailAlert("Those passwords don't match. Try again.");

    }else{

      if(!checkString){ 
        this.registerFailAlert("Please make sure you filled in all fields."); 
        return;
       }




      //Firebase function that creates a new this.Form.
      return this.afAuth.auth.createUserWithEmailAndPassword(this.Form.email, this.Form.password)
        .then((user) => {

          //Push to another view page.
          this.navCtrl.push(ViewPage);
          console.log('Authenticated.')

        })
        .catch((error) => {
          console.log(error)
          this.registerFailAlert(error.message)
        }); //Throw an error just in case.

    }


  }



  //An alert that is presented showing that the login failed.
  loginFailAlert() {

    //Alert to show that the login failed.
    let alert = this.alertCtrl.create({
      title: 'Sorry.',
      subTitle: 'There was an error logging in.',
      buttons: ['Ok']
    });

    //Present the alert.
    alert.present();
  }


  //An alert that is presented showing that the login failed.
  registerFailAlert(str) {

    //Alert to show that the login failed.
    let alert = this.alertCtrl.create({
      title: 'Sorry.',
      subTitle: str,
      buttons: ['Ok']
    });

    //Present the alert.
    alert.present();
  }


  loginClicked(){
    this.navCtrl.push(LoginPage);
  }

}
