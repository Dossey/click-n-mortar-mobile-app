import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { StartingPage } from '../starting/starting'
import { Page1Page } from '../page1/page1'
import { Page2Page } from '../page2/page2'
import { Page3Page } from '../page3/page3'

/**
 * Generated class for the ViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view',
  templateUrl: 'view.html',
})
export class ViewPage {

  // this tells the tabs component which Pages
// should be each tab's root Page
    tab1Root = Page1Page;
    tab2Root = Page2Page;
    tab3Root = Page3Page;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPage');
  }

}
