import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login'
import { ViewPage } from '../view/view'
import { AlertController } from 'ionic-angular'
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

/**
 * Generated class for the StartingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-starting',
  templateUrl: 'starting.html',
})
export class StartingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private fb: Facebook, private alertCtrl : AlertController) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad StartingPage');
  }

  // Show the login alert.
  showLoginAlert(){

    //Alert to show that the login failed.
    let alert = this.alertCtrl.create({
      title: 'Sorry.',
      subTitle: 'There was an error logging in.',
      buttons: ['Ok']
    });

    //Present the alert.
    alert.present();

  }



  //When the facebook button is clicked.
  facebookClicked(){

    //Login function for facebook.
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {

        //If the login is successful, push to the main page.
        this.navCtrl.push(ViewPage)

      })
      .catch(e =>{

        //Give an alert that the facebook login didn't work.
        this.showLoginAlert()

      });


  }

  //Function acts when email button is clicked.
  emailClicked(){

    //Push to the email/password login page.
    this.navCtrl.push(LoginPage);

  }

}
