import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HowToRedeemPage } from './how-to-redeem';

@NgModule({
  declarations: [
    HowToRedeemPage,
  ],
  imports: [
    IonicPageModule.forChild(HowToRedeemPage),
  ],
})
export class HowToRedeemPageModule {}
